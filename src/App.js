import React from 'react';
import Dropdown from './components/CustomSelect';
import Dropdown1 from './components/CustomSelect1';
import './App.css';

function App() {
const list = {
  data: [
    {
        id: 0,
        title: 'value1',
    },
    {
      id: 1,
      title: 'value2',
    },
    {
      id: 2,
      title: 'value3',
    },
    {
      id: 3,
      title: 'value4',
    },
    {
      id: 4,
      title: 'value5',
    },
    {
      id: 5,
      title: 'value6',
    }
  ]
}
  
  return (
    <div className="App">
      <Dropdown
        title="Select..."
        list={list.data}
      />
      <div>with hook</div>
       <Dropdown1
        title="Select..."
        list={list.data}
      />
      </div>
  );
}

export default App;
