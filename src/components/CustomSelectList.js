import React, {useState, useEffect, useRef} from 'react';
import classNames from "classnames";

const  CustomSelectList = ({handleSelect, setHeaderTitle, headerTitle= 'Select...', list}) => {
  const [cursor, setCursor] = useState(0);
  const ref = useRef();
  const handleKeyDown = (e) => {
      const {keyCode} = e;
      if (keyCode === 38 && cursor > 0) {
        setCursor(prevState => --prevState);
      } else if (keyCode === 40 && cursor < list.length - 1) {
        setCursor( prevState => ++prevState);
      }
      else if (keyCode === 13) {
        const el = ref.current.querySelector('li[class~="active"]');
        if (el) {
          setHeaderTitle(el.dataset.title);
        }
      }
      return;
  }
  useEffect(()=>{
    document.addEventListener("keydown", handleKeyDown);
    return () => {
      document.removeEventListener("keydown", handleKeyDown);
    }
  });
  return (
    <ul ref={ref}>
      {list.map((item,i) => (
      <li className={classNames({ selected:item.title === headerTitle, active: cursor-1 === i})} 
        data-title={item.title} key={item.id} onClick={handleSelect} >
          {item.title}
      </li>
      ))}
    </ul>
  );
}
export default CustomSelectList;