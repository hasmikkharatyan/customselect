import React, {useRef, useState} from 'react';
import '../styles/customSelect.scss';
import  CustomSelectList from './CustomSelectList';
import CustomSelectHeader from './CustomSelectHeader';
import useOutsideClick from './customHooks/outsideClick';

const  Dropdown1 = (props) => {
    const {list, title, headerTitle:headerTitleFromProps} = props;
    const ref = useRef();
    const [openState, setOpenState] = useState(false);
    const [headerTitle, setHeaderTitle] = useState(headerTitleFromProps);
    
    useOutsideClick(ref, (e) => {
        if(!e.target.closest(".close-icon")){
        setOpenState(false);
        }
    });

    const handleClose = () => {
        setHeaderTitle(headerTitleFromProps);
    }
    const toggleList = (e) => {
        if(!e.target.closest(".close-icon")){
            setOpenState(state => {
                return !state;
            });
        }
    }
    const handleSelect = (e) => {
        setHeaderTitle(e.target.dataset.title);
    }

    return(
        <div className="wrapper" ref={ref}>
          <CustomSelectHeader 
          toggleList={toggleList}
          handleClose={handleClose}
          headerTitle={headerTitle}
          listOpen={openState}
          title={title}
          />
          {openState && <CustomSelectList 
            handleSelect={handleSelect}
            headerTitle={headerTitle}
            setHeaderTitle={setHeaderTitle}
            list={list}
          />}
        </div>
      )
}
export default Dropdown1;