import React from 'react';
import '../styles/customSelect.scss';
import  CustomSelectList from './CustomSelectList';
import CustomSelectHeader from './CustomSelectHeader';

class Dropdown extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      listOpen: false,
      headerTitle: this.props.title,
      cursor:0
    }
    this.myRef = React.createRef();
  }

  componentDidMount(){
    document.addEventListener('click', this.handleOutsideClick, false);
    // document.addEventListener("keydown", this.handleKeyDown);
  }
  componentWillUnmount(){
    document.removeEventListener('click', this.handleOutsideClick, false);
    // document.removeEventListener("keydown", this.handleKeyDown);
  }

  // handleKeyDown=(e)=> {
  //   const { cursor, listOpen } = this.state;
  //   const {list} = this.props;
  //   if (listOpen) {
  //     const {keyCode} = e;
  //     if (keyCode === 38 && cursor > 0) {
  //       this.setState(prevState => ( {cursor: --prevState.cursor}));
  //     } else if (keyCode === 40 && cursor < list.length - 1) {
  //       this.setState( prevState => ( {cursor: ++prevState.cursor}));
  //     }
  //     else if (keyCode === 13) {
  //       const el = this.myRef.current.querySelector('ul li[class~="active"]');
  //       if (el) {
  //         this.setState({headerTitle:el.dataset.title});
  //       }
  //     }
  //     return;
  //   }
  // }

  setListState = (listOpen) => {
    this.setState(prevState => ({
      listOpen: listOpen,
      ...(!listOpen && {cursor:0})
    }))
  }

  toggleList = () =>{
    this.setState(prevState => ({
      listOpen: !prevState.listOpen
    }))
  }

  handleOutsideClick=(e)=> {
    if (this.myRef && this.myRef.current.contains(e.target) 
    || e.target.closest(".close-icon")) {
      return;
    }
    this.setListState(false);
  }

  handleClose = () => {
    this.setState({headerTitle: this.props.title, listOpen: false});
  }

  handleSelect = (e) => {
   this.setState({
       headerTitle:e.target.dataset.title,
       listOpen: false
    });
  }
  setHeaderTitle = (title) => {
    this.setState({
      headerTitle:title,
      listOpen: false
   });
  }

  render(){
    const{list, title} = this.props;
    const{listOpen, headerTitle,cursor} = this.state;
    return(
      <div className="wrapper" ref={this.myRef}>
        <CustomSelectHeader 
        toggleList={this.toggleList}
        handleClose={this.handleClose}
        headerTitle={headerTitle}
        listOpen={listOpen}
        title={title}
        />
        {listOpen && <CustomSelectList 
          handleSelect={this.handleSelect}
          setHeaderTitle={this.setHeaderTitle}
          cursor={cursor}
          headerTitle={headerTitle}
          list={list}
        />}
      </div>
    )
  }
}

export default Dropdown;