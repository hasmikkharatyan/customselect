import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleUp, faAngleDown, faTimes } from '@fortawesome/free-solid-svg-icons';

const CustomSelectHeader = ({toggleList, handleClose, headerTitle = 'Select...', title, listOpen }) => (
  <div className="header" onClick={toggleList}>
    <div className="header-title">{headerTitle}</div>
    {title != headerTitle &&
    <div className="close-icon" onClick={handleClose}>
    <FontAwesomeIcon icon={faTimes}/>
    </div>
    }
    <span className="separator"></span>
    <div className="up-down-icon">
    {listOpen
        ?<FontAwesomeIcon icon={faAngleUp} />
        :<FontAwesomeIcon icon={faAngleDown} />
    }
    </div>
  </div>
);
export default CustomSelectHeader;

